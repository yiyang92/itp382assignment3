﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieController : MonoBehaviour {

	public float movespeed;
	public float turnSpeed;
	public AudioClip enemyContactSound;
	public AudioClip catContactSound;
	AudioSource audio;


	private Vector3 moveDirection;
	private List<Transform> congaLine = new List<Transform>();
	private int lives = 3;



	[SerializeField]
	private PolygonCollider2D[]colliders;
	private int currentColliderIndex = 0;
	private bool isInvincible = false;
	private float timeSpentInvincible;

	private void EnforceBounds()
	{
		// 1
		Vector3 newPosition = transform.position; 
		Camera mainCamera = Camera.main;
		Vector3 cameraPosition = mainCamera.transform.position;

		// 2
		float xDist = mainCamera.aspect * mainCamera.orthographicSize; 
		float xMax = cameraPosition.x + xDist;
		float xMin = cameraPosition.x - xDist;
		float yMax = mainCamera.orthographicSize;

		// TODO vertical bounds

		if (newPosition.y < -yMax || newPosition.y > yMax) {
			newPosition.y = Mathf.Clamp( newPosition.y, -yMax, yMax );
			moveDirection.y = -moveDirection.y;
		}

		// 3
		if ( newPosition.x < xMin || newPosition.x > xMax ) {
			newPosition.x = Mathf.Clamp( newPosition.x, xMin, xMax );
			moveDirection.x = -moveDirection.x;
		}

		// 4
		transform.position = newPosition;
	}

	void OnTriggerEnter2D( Collider2D other )
	{
		if(other.CompareTag("cat")) {

			Transform followTarget = congaLine.Count == 0 ? transform : congaLine[congaLine.Count-1];
			other.transform.parent.GetComponent<CatController>().JoinConga( followTarget, movespeed, turnSpeed );
			congaLine.Add( other.transform );
			audio.PlayOneShot(catContactSound);

			if (congaLine.Count >= 5) {
				Application.LoadLevel("WinScene");
			}


		}
		else if(!isInvincible && other.CompareTag("enemy")) {
			isInvincible = true;
			timeSpentInvincible = 0;
			audio.PlayOneShot(enemyContactSound);

			for( int i = 0; i < 2 && congaLine.Count > 0; i++ )
			{
				int lastIdx = congaLine.Count-1;
				Transform cat = congaLine[ lastIdx ];
				congaLine.RemoveAt(lastIdx);
				cat.parent.GetComponent<CatController>().ExitConga();
			}
			if (--lives <= 0) {
				Application.LoadLevel("LoseScene");
			}

		}
	}

	public void SetColliderForSprite( int spriteNum )
	{
		colliders[currentColliderIndex].enabled=false;
		currentColliderIndex = spriteNum;
		colliders[currentColliderIndex].enabled=true;
	}

	// Use this for initialization
	void Start () {
		moveDirection = Vector3.right;
		audio = GetComponent<AudioSource>();


	}
	
	// Update is called once per frame
	void Update () {
		//1
		Vector3 currentPosition = transform.position;
		//2
		if (Input.GetButton ("Fire1")) {
			Vector3 moveToward = Camera.main.ScreenToWorldPoint (Input.mousePosition);

			moveDirection = moveToward - currentPosition;
			moveDirection.z = 0;
			moveDirection.Normalize ();
		}

		Vector3 target = moveDirection * movespeed + currentPosition;
		transform.position = Vector3.Lerp (currentPosition, target, Time.deltaTime);

		float targetAngle = Mathf.Atan2 (moveDirection.y, moveDirection.x) * Mathf.Rad2Deg;
		transform.rotation = 
			Quaternion.Slerp (transform.rotation,
			Quaternion.Euler (0, 0, targetAngle),
			turnSpeed * Time.deltaTime);

		EnforceBounds();

		if (isInvincible)
		{
			//2
			timeSpentInvincible += Time.deltaTime;

			//3
			if (timeSpentInvincible < 3f) {
				float remainder = timeSpentInvincible % .3f;
				gameObject.GetComponent<SpriteRenderer> ().enabled = remainder > .15f; 
			}
			//4
			else {
				gameObject.GetComponent<SpriteRenderer> ().enabled = true;
				isInvincible = false;
			}
		}
	}
}
