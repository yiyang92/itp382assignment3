﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

	public float speed = -1;
	private Rigidbody2D rigi;
	private Transform spawnPoint;


	void OnBecameInvisible()
	{
		float yMax = Camera.main.orthographicSize - 0.5f;
		transform.position = new Vector3( spawnPoint.position.x, 
			Random.Range(-yMax, yMax), 
			transform.position.z );
	}

	// Use this for initialization
	void Start () {

		rigi = GetComponent<Rigidbody2D>();

	
		rigi.velocity = new Vector2(speed, 0);

		spawnPoint = GameObject.Find("SpawnPoint").transform;

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
